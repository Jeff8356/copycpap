# CopyCPAP

AppleScript code to copy CPAP data from a FlashAir WiFi SD card without the need for Java.

Many Mac users that updated to Big Sur found that Java had been broken.  Their favorite app to copy their CPAP data from their FlashAir card no longer worked and an alternative was born from that.
This simple script uses native Mac commands and has no reliance on Java.  It uses rsync to copy data from the FlashAir card to your computer or NAS.

